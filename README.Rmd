---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, echo = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "README-"
)
```

# About

`acs2010s` is an naive attempt to include large ACS data as an R data package. 
So far it contains ACS 2010--2014, which are extracted from IPUMS-USA.

# Installation

```R
# install.packages("devtools")
devtools::install_bitbucket("jjchern/acs2010s")
```

