
<!-- README.md is generated from README.Rmd. Please edit that file -->
About
=====

`acs2010s` is an naive attempt to include large ACS data as an R data package. So far it contains ACS 2010--2014, which are extracted from IPUMS-USA.

Installation
============

``` r
# install.packages("devtools")
devtools::install_github("jjchern/acs2010s")
```
